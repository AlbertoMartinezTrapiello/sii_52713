#include "DatosMemCompartida.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>


int main (void)
{
	
	DatosMemCompartida * DMC;

	int fd = open ("DatosCompartidos", O_RDWR,0777);
	if(fd < 0)
	{
		perror("Error en la apertura de DatosCompartidos de lectura");
		return (1);
	}
	
	
	DMC = (DatosMemCompartida *) mmap(NULL, sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);	
	if( DMC == MAP_FAILED)
	{
                perror("Error en la proyeccion del fichero del Mundo en modo lectura");
                close(fd);
 		return 1;
        }

	//printf("%f y %f \n", pDMC->esfera.centro.y, pDMC->esfera.centro.x);
	

	
	close(fd);
	while (1)
	{
		
		//lógica
	
		float centro_raqueta = (DMC->raqueta1.y2 - DMC->raqueta1.y1);
		float centro_esfera = DMC->esfera.centro.y;
		if (centro_raqueta > centro_esfera)
		{
			DMC->accion = -1;
			
		}
		if (centro_raqueta < centro_esfera)
		{
			DMC->accion = 1;
		}		
		if(centro_raqueta == centro_esfera)
		{
			DMC->accion = 0;
		}		
		
		usleep(25);
	}
	return 0;
}


