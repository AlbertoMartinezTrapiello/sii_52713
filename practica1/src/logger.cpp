#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

#define NUM_MAX 9


struct message 
{
	char nombre_jug[NUM_MAX];	
	int points = 0;
};


int main (void)
{
	int fd;	
	

	unlink("FIFO");//se carga el FIFO en caso de que se haya creado antes 
	/* crea el FIFO */
	if (mkfifo("FIFO", 0600)<0) {//Crea el FIFO 
		perror("No puede crearse el FIFO");
		return(1);
	}
	/* Abre el FIFO */
	if ((fd=open("FIFO", O_RDONLY))<0) {//Abre el FIFO para leer su contenido
		perror("No puede abrirse el FIFO");
		return(1);
	}
	//struct stat fifo;
	//fstat(fd, &fifo);
	
	message m;
	m.points = 0; 
	while (read(fd, &m, sizeof(m))==sizeof(m))//debería leer un número entero de mensajes dejados por el mundo. Estos deberían guardarse correctamente en el mensaje m
	{
	
		//printf("%s marca 1 punto, lleva un total de %d\n", m.nombre_jug, (++m.points));//Con el pritnf espera a cerrar el fd para imprime todas las veces seguidas, debe de guardarlo en el buffer
		printf("%s marca 1 punto, lleva un total de %d\n", m.nombre_jug, (++m.points));
		/*char cadena[50];
		int read = sprintf(cadena, "%s marca 1 punto, lleva un total de %d\n", m.nombre_jug, &(++m.points));	
		write(1,"Read %d\n", &read);
		write(1,cadena,read*sizeof(char));*/
	}
	close(fd);
	unlink("FIFO");
	return 0;
}
